function formSubmit () {

	// get these values by element ID
	var name = document.getElementById('name').value;
	var phone = document.getElementById('phone').value;
	var email = document.getElementById('email').value;
	var reason = document.getElementById('reason').value;
	var additionalInfo = document.getElementById('additional').value;

	// get all the checkbox inputs
	var choices = document.getElementsByName('choice');

	if (name === '') {
		// get here if name is left blank
		return alert('Name value cannot be left blank.');
	}

	if (phone === '' && email === '') {
		// get here if both phone and email are empty
		return alert('Either phone or email must be filled in.');
	}

	if (reason === 'Other' && additionalInfo == '') {
		// get here if reason is 'Other' and additional info is left blank
		return alert('Please fill in additional info if reason is "Other".');
	}

	var checkedBoxes = []; // create an empty array

	for (var i = 0; i < choices.length; i++) {
		// loop through each checkbox input element
		
		if (choices[i].checked) {
			// if the checkbox is checked, add it to the "checkedBoxes" array
			checkedBoxes.push(choices[i]);
		}
	}

	if (checkedBoxes.length == 0) {
		// get here if no boxes are checked
		return alert('Please select a day for us to contact you.');
	}

}


