
function rollDice () {
	return Math.floor(Math.random() * 6) + 1;
} 

function bet () {
	var startingBet = parseInt(document.getElementById('bet-amount').value),
		currentBalance = startingBet,
		maxBalance = 0,
		maxBalanceRollCount = 0,
		counter = 0;

	while (currentBalance > 0) {
		var dice1 = rollDice(),
			dice2 = rollDice(),
			total = dice1 + dice2;

		if (total === 7) {
			currentBalance += 4;
		} else {
			currentBalance--;
		}

		counter++;

		if (maxBalance < currentBalance) {
			// this is max balance
			maxBalance = currentBalance;
			maxBalanceRollCount = counter;
		}
	}

	document.getElementById('starting-bet').innerHTML = startingBet;
	document.getElementById('roll-counter').innerHTML = counter;
	document.getElementById('max-balance').innerHTML = maxBalance;
	document.getElementById('max-balance-roll-count').innerHTML = maxBalanceRollCount;
}